module gitlab.com/gitlab-com/gl-infra/jsonnet-tool

go 1.21.6

require (
	github.com/fatih/color v1.16.0
	github.com/google/go-jsonnet v0.20.0
	github.com/hexops/gotextdiff v1.0.3
	github.com/kr/text v0.2.0
	github.com/spf13/cobra v1.8.0
	gopkg.in/alessio/shellescape.v1 v1.0.0-20170105083845-52074bc9df61
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/alessio/shellescape v1.4.2 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/sergi/go-diff v1.3.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.14.0 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	sigs.k8s.io/yaml v1.3.0 // indirect
)
